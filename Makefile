CXX = $(PWD)/llvm/bin/clang++
CXXFLAGS += -O2 -Wall -Wextra -g
CXXFLAGS += --target=x86_64-elf
CXXFLAGS += -ffreestanding -mno-red-zone -fno-exceptions -fno-rtti -fno-threadsafe-statics
CXXFLAGS += -std=c++17
CXXFLAGS += -MD
CXXFLAGS += -D__ELF__ -D_LIBCPP_HAS_NO_THREADS

CXXFLAGS += -I$(PWD)/kernel
CXXFLAGS += -I$(PWD)/newlib/include
CXXFLAGS += -I$(PWD)/llvm/include/c++/v1
CXXFLAGS += -I$(PWD)/llvm/lib/clang/11.1.0/include

LD = $(PWD)/llvm/bin/ld.lld
LDFLAGS += --entry KernelMain --image-base 0x100000 --static
LDFLAGS += -L$(PWD)/newlib/lib -L$(PWD)/llvm/lib
LDFLAGS += -lc -lg -lm -lnosys
LDFLAGS += -lc++

AS = nasm -f elf64

.PHONY: all
all: kernel/kernel.elf

MOD_LIST := pci log usb

CPP_SRCS := $(wildcard kernel/*.cpp)
CPP_SRCS += $(wildcard kernel/pci/*.cpp)
CPP_SRCS += $(wildcard kernel/log/*.cpp)
CPP_SRCS += $(foreach mod,$(MOD_LIST),$(wildcard kernel/$(mod)/*.cpp))

ASM_SRCS := $(wildcard kernel/pci/*.asm)

OBJS := $(CPP_SRCS:.cpp=.o) $(ASM_SRCS:.asm=.o)
DEPS := $(CPP_SRCS:.cpp=.d)

kernel/%.o: kernel/%.cpp Makefile
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

kernel/%.o: kernel/%.asm Makefile
	$(AS) $< -o $@

kernel/kernel.elf: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^

.PHONY: img
img:
	cargo build
	sudo mount -o loop disk.img mnt
#	sudo cp -i edk2/Build/MikanLoaderX64/DEBUG_GCC5/X64/Loader.efi mnt/EFI/BOOT/BOOTX64.EFI
	sudo cp -i Loader.efi mnt/EFI/BOOT/BOOTX64.EFI
	sudo cp target/debug/reikou mnt/kernel.elf
	sudo umount mnt

.PHONY: run
run:
	qemu-system-x86_64 \
		-drive if=pflash,file=./mikanos-build/devenv/OVMF_CODE.fd \
		-drive if=pflash,file=./mikanos-build/devenv/OVMF_VARS.fd \
		-device nec-usb-xhci,id=xhci \
		-device usb-mouse -device usb-kbd \
		-hda disk.img \
		-monitor stdio

.PHONY: clean
clean:
	$(RM) kernel/kernel.elf
	$(RM) $(OBJS)
	$(RM) $(DEPS)

-include $(DEPS)
