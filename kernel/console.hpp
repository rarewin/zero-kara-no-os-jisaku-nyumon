#ifndef CONSOLE_H_INCLUDED
#define CONSOLE_H_INCLUDED

#include "graphics.hpp"

class Console
{
      public:
	Console(const Console &) = delete;		 // コピーコンストラクタ
	Console &operator=(const Console &) = delete;	 // コピー代入演算子
	Console(Console &&) = delete;			 // ムーブコンストラクタ
	Console &operator=(Console &&) = delete;	 // ムーブ代入演算子

	static constexpr int kRows = 30;
	static constexpr int kColumns = 100;

	static void init(PixelWriter &writer, const PixelColor &fg_color, const PixelColor &bg_color)
	{
		Console::get_inst_impl(&writer, &fg_color, &bg_color);
	}

	static Console &get_inst()
	{
		return Console::get_inst_impl();
	}

	void put_string(const char *s);

      private:
	~Console() = default;

	Console(PixelWriter *writer, const PixelColor *fg_color, const PixelColor *bg_color)
		: writer(*writer), fg_color(*fg_color), bg_color(*bg_color), buffer(), cursor_row(0), cursor_column(0)
	{
	}

	static Console &get_inst_impl(PixelWriter *writer = nullptr, const PixelColor *fg_color = nullptr,
				      const PixelColor *bg_color = nullptr)
	{
		static Console inst{writer, fg_color, bg_color};
		return inst;
	}

	void newline();

	PixelWriter &writer;
	const PixelColor fg_color;
	const PixelColor bg_color;
	char buffer[kRows][kColumns + 1];
	int cursor_row;
	int cursor_column;
};
#endif
