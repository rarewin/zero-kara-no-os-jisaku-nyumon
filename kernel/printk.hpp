#ifndef PRINTK_H_INCLUDED
#define PRINTK_H_INCLUDED
int printk(const char *format, ...);
#endif
