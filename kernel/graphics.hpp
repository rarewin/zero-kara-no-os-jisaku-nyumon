#ifndef GRAPHICS_H_INCLUDED
#define GRAPHICS_H_INCLUDED

#include <cstddef>

#include "frame_buffer_config.hpp"

template <typename T> struct Vector2D {
	T x, y;

	template <typename U> Vector2D<T> &operator+=(const Vector2D<U> &rhs)
	{
		this->x += rhs.x;
		this->y += rhs.y;
		return *this;
	}
};

struct PixelColor {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

class PixelWriter
{
      public:
	PixelWriter(const FrameBufferConfig &config) : config(config)
	{
	}
	virtual ~PixelWriter() = default;
	virtual void Write(int x, int y, const PixelColor &c) = 0;

	void *operator new(size_t /* size */, void *buf)
	{
		return buf;
	}

	void operator delete(void * /* obj */)
	{
	}

      protected:
	uint8_t *PixelAt(int x, int y)
	{
		return this->config.frame_buffer + 4 * (this->config.pixels_per_scan_line * y + x);
	}

      private:
	const FrameBufferConfig &config;
};

class RgbResv8BitPerColorPixelWriter : public PixelWriter
{
      public:
	using PixelWriter::PixelWriter;

	virtual void Write(int x, int y, const PixelColor &c) override;
};

class BgrResv8BitPerColorPixelWriter : public PixelWriter
{
      public:
	using PixelWriter::PixelWriter;

	virtual void Write(int x, int y, const PixelColor &c) override;
};

void FillRectangle(PixelWriter &writer, const Vector2D<int> &pos, const Vector2D<int> &size, const PixelColor &c);

void DrawRectangle(PixelWriter &writer, const Vector2D<int> &pos, const Vector2D<int> &size, const PixelColor &c);
#endif
