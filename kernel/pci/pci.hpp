#ifndef PCI_H_INCLUDED
#define PCI_H_INCLUDED

#include <array>
#include <cstddef>
#include <cstdint>

/* asmfunc.asm */
extern "C" {
void ioout32(uint16_t addr, uint32_t data);
uint32_t ioin32(uint16_t addr);
}

namespace
{
constexpr size_t TABLE_SIZE = 16;
}

enum class PciResult {
	Success,
	TableFull,
};

struct PciDevice {
      public:
	PciDevice(uint8_t bus, uint8_t device, uint8_t function, uint8_t header_type)
		: bus(bus), device(device), function(function), header_type(header_type)
	{
	}

	PciDevice() : bus(0xffu), device(0xffu), function(0xffu), header_type(0xffu)
	{
	}

	uint8_t get_bus()
	{
		return this->bus;
	}

	uint8_t get_device()
	{
		return this->device;
	}

	uint8_t get_function()
	{
		return this->function;
	}

	uint16_t get_vendor_id();

	uint64_t get_base_addr(int index);

      private:
	uint8_t bus;
	uint8_t device;
	uint8_t function;
	uint8_t header_type;
};

class PciDriver
{
      public:
	PciDriver(const PciDriver &) = delete;		     // コピーコンストラクタ
	PciDriver &operator=(const PciDriver &) = delete;    // コピー代入演算子
	PciDriver(PciDriver &&) = delete;		     // ムーブコンストラクタ
	PciDriver &operator=(PciDriver &&) = delete;	     // ムーブ代入演算子

	static PciDriver &get_inst()
	{
		static PciDriver inst{};

		return inst;
	}

	/**
	 * @brief バススキャン
	 */
	PciResult scan_all_bus();

	PciDevice *search_by_class(uint8_t base, uint8_t sub, uint8_t interface);

      private:
	PciDriver() /* : device_table() */
	{
	}
	~PciDriver() = default;

	PciResult scan_bus(uint8_t bus);
	PciResult scan_function(uint8_t bus, uint8_t device, uint8_t function);
	PciResult scan_device(uint8_t bus, uint8_t device);

	PciResult push_device(uint8_t bus, uint8_t device, uint8_t function, uint8_t header_type);

	std::array<PciDevice, TABLE_SIZE> device_table;
	size_t device_table_size = 0;
};
#endif
