	bits 64
	section .text

	global ioout32		; void ioout32(uint16_t addr, uint32_t data)
ioout32:
	mov	dx, di
	mov	eax, esi
	out	dx, eax
	ret

	global ioin32		; uint32_t ioin32(uint16_t addr)
ioin32:
	mov dx, di
	in eax, dx
	ret
