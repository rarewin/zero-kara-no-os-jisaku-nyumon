#include <cstdbool>
#include <cstdint>

#include <log/log.hpp>

#include "pci.hpp"

namespace
{
constexpr uint16_t CONFIG_ADDRESS = 0x0cf8u;
constexpr uint16_t CONFIG_DATA = 0x0cfcu;
}    // namespace

/**
 * @brief CONFIG_ADDRESSの値を生成する
 *
 *    [31]  configuration enable (CFGE)
 * [30:24]  reserved
 * [23:16]  bus number
 * [15:11]  device number
 * [10: 8]  function
 * [ 7: 2]  register number
 * [ 1: 0]  reserved
 */
static inline uint32_t make_address(uint8_t bus, uint8_t device, uint8_t function, uint8_t reg_addr)
{
	return (1u << 31)    // enable bit
	       | (bus << 16) | ((device & 0x1fu) << 11) | ((function & 0x07u) << 8) | (reg_addr & 0xfcu);
}

static bool is_single_function_device(uint8_t header_type)
{
	return ((header_type & (1u << 7)) == 0);
}

static void write_address(uint32_t address)
{
	ioout32(CONFIG_ADDRESS, address);
}

static void write_data(uint32_t value)
{
	ioout32(CONFIG_DATA, value);
}

static uint32_t read_data()
{
	return ioin32(CONFIG_DATA);
}

static uint32_t read_reg(uint8_t bus, uint8_t device, uint8_t function, uint8_t addr)
{
	write_address(make_address(bus, device, function, addr));
	uint32_t ret = read_data();
	write_address(0);

	return ret;
}

static uint16_t read_vendor_id(uint8_t bus, uint8_t device, uint8_t function)
{
	return read_reg(bus, device, function, 0x00u) & 0xffffu;
}

static uint8_t read_header_type(uint8_t bus, uint8_t device, uint8_t function)
{
	return (read_reg(bus, device, function, 0x0cu) >> 16) & 0xffu;
}

static uint8_t read_base_class(uint8_t bus, uint8_t device, uint8_t function)
{
	return (read_reg(bus, device, function, 0x08u) >> 24) & 0xffu;
}

static uint8_t read_sub_class(uint8_t bus, uint8_t device, uint8_t function)
{
	return (read_reg(bus, device, function, 0x08u) >> 16) & 0xffu;
}

static uint8_t read_interface(uint8_t bus, uint8_t device, uint8_t function)
{
	return (read_reg(bus, device, function, 0x08u) >> 8) & 0xffu;
}

static uint32_t read_bus_number(uint8_t bus, uint8_t device, uint8_t function)
{
	return read_reg(bus, device, function, 0x18u);
}

PciResult PciDriver::scan_function(uint8_t bus, uint8_t device, uint8_t function)
{
	uint8_t header_type = read_header_type(bus, device, function);

	PciResult ret;

	// @todo add device
	if ((ret = this->push_device(bus, device, function, header_type)) != PciResult::Success) {
		return ret;
	}

	uint8_t base = read_base_class(bus, device, function);
	uint8_t sub = read_sub_class(bus, device, function);

	if (base == 0x06u && sub == 04u) {
		uint8_t secondary_bus = (read_bus_number(bus, device, function) >> 8) & 0xffu;

		this->scan_bus(secondary_bus);
	}

	return PciResult::Success;
}

PciResult PciDriver::scan_device(uint8_t bus, uint8_t device)
{
	PciResult ret;

	if ((ret = this->scan_function(bus, device, 0)) != PciResult::Success) {
		return ret;
	}

	if (is_single_function_device(read_header_type(bus, device, 0))) {
		return PciResult::Success;
	}

	for (uint8_t function = 1; function < 8; function++) {
		if (read_vendor_id(bus, device, function) == 0xffffu) {
			continue;
		}

		if ((ret = this->scan_function(bus, device, function)) != PciResult::Success) {
			return ret;
		}
	}

	return PciResult::Success;
}

PciResult PciDriver::scan_bus(uint8_t bus)
{
	for (uint8_t device = 0; device < 32; device++) {

		if (read_vendor_id(bus, device, 0) == 0xffffu) {
			continue;
		}

		PciResult ret;

		if ((ret = scan_device(bus, device)) != PciResult::Success) {
			return ret;
		}
	}

	return PciResult::Success;
}

/**
 * @brief バススキャン
 */
PciResult PciDriver::scan_all_bus()
{
	uint8_t header_type = read_header_type(0, 0, 0);

	if (is_single_function_device(header_type)) {
		return this->scan_bus(0);
	}

#if 0
	for (uint8_t bus = 1; bus < 8; bus++) {
		if (read_vendor_id(bus, 0, 0) == 0xffffu) {
			continue;
		}

		PciResult ret;

		if ((ret = this->scan_bus(bus)) != PciResult::Success) {
			return ret;
		}
	}
#endif

	return PciResult::Success;
}

PciResult PciDriver::push_device(uint8_t bus, uint8_t device, uint8_t function, uint8_t header_type)
{
	if (device_table_size == device_table.size()) {
		return PciResult::TableFull;
	}

	Log &log = Log::get_inst();
	log.log(LogLevel::INFO, "found device  bus: 0x%02x, device: 0x%02x, function: 0x%02x, header_type: 0x%02x\n",
		bus, device, function, header_type);

	this->device_table[this->device_table_size++] = PciDevice{bus, device, function, header_type};

	return PciResult::Success;
}

PciDevice *PciDriver::search_by_class(uint8_t base, uint8_t sub, uint8_t interface)
{
	Log &log = Log::get_inst();

	for (size_t i = 0; i < this->device_table_size; i++) {
		auto &d = this->device_table[i];

		uint8_t bus = d.get_bus();
		uint8_t device = d.get_device();
		uint8_t function = d.get_function();

		uint8_t d_base = read_base_class(bus, device, function);
		uint8_t d_sub = read_sub_class(bus, device, function);
		uint8_t d_interface = read_interface(bus, device, function);

		log.log(LogLevel::DEBUG,
			"checking device  bus: 0x%02x, device: 0x%02x, function: 0x%02x\n"
			"             --> base: 0x%02x, sub: 0x%02x, interface: 0x%02x\n",
			bus, device, function, d_base, d_sub, d_interface);
		if (d_base == base && d_sub == sub && d_interface == interface) {
			return &d;
		}
	}

	return nullptr;
}

uint16_t PciDevice::get_vendor_id()
{
	return read_vendor_id(this->bus, this->device, this->function);
}

/**
 *
 * <https://wiki.osdev.org/PCI#Base_Address_Registers>
 */
uint64_t PciDevice::get_base_addr(int index)
{
	// header type 00のみサポート
	if (this->header_type != 0x00) {
		return 0;
	}

	// BAR5まで
	if (index > 5) {
		return 0;
	}

	uint8_t addr = 0x10 + 0x4 * index;

	uint32_t bar = read_reg(this->bus, this->device, this->function, addr);

	// I/O Spaceだったらエラー
	if ((bar & 1) != 0) {
		return 0;
	}

	// 32-bit
	if (((bar >> 1) & 0x3) == 0x00) {
		return (bar & 0xfffffff0u);
	}

	// 次のBARをみる
	if (index > 4) {
		return 0;
	}

	uint64_t bar_upper = read_reg(this->bus, this->device, this->function, addr + 4);

	return ((bar_upper << 32) | (bar & 0xfffffff0u));
}
