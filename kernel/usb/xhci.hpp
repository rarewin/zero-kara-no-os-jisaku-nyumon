#ifndef XHCI_H_INCLUDED
#define XHCI_H_INCLUDED

#include <cstdint>

namespace usb
{

enum class XhciDeviceResult {
	OK,
	FAILED,
};

class XhciDevice
{
      public:
	XhciDevice(uintptr_t base_addr);

	XhciDeviceResult reset();

	void debug();

      private:
	uintptr_t base_addr;
	uintptr_t operational_base;

	void set_usbcmd(uint32_t value)
	{
		*(reinterpret_cast<uint32_t *>(this->operational_base)) = value;
	}

	uint32_t get_usbcmd()
	{
		return *(reinterpret_cast<uint32_t *>(this->operational_base));
	}

	uint32_t get_usbsts()
	{
		return *(reinterpret_cast<uint32_t *>(this->operational_base + 4));
	}
};

}    // namespace usb
#endif
