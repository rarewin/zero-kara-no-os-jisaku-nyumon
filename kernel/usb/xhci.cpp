#include <cinttypes>

#include <log/log.hpp>

#include "xhci.hpp"

namespace usb
{
XhciDevice::XhciDevice(uintptr_t base_addr) : base_addr(base_addr)
{
	auto &log = Log::get_inst();

	uint8_t *p = reinterpret_cast<uint8_t *>(this->base_addr);

	uint8_t caplength = p[0];
	log.log(LogLevel::INFO, "CAPLENGTH: 0x%02hx\n", caplength);

	this->operational_base = this->base_addr + caplength;
	log.log(LogLevel::INFO, "Operational Base: %016" PRIx64 "\n", this->operational_base);
}

XhciDeviceResult XhciDevice::reset()
{
	auto &log = Log::get_inst();

	log.log(LogLevel::DEBUG, "reset xhci.");

	if ((this->get_usbsts() & 1) != 1) {
		log.log(LogLevel::DEBUG, "failed. (HCH is not zero) : %08x\n", this->get_usbsts());
		return XhciDeviceResult::FAILED;
	}

	this->set_usbcmd(this->get_usbcmd() | (1u << 1));    // set HCRST

	log.log(LogLevel::DEBUG, ".");

	while ((this->get_usbcmd() & (1u << 1)) != 0)	 // wait until HCRST=1
	{
		;
	}

	log.log(LogLevel::DEBUG, ".");

	while ((this->get_usbsts() & (1u << 11)) != 0)	  // wait until CNR=1
	{
		;
	}

	log.log(LogLevel::DEBUG, "OK!\n");

	return XhciDeviceResult::OK;
}

void XhciDevice::debug()
{
}
}    // namespace usb
