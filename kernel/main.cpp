#include <cinttypes>
#include <cstdarg>
#include <cstddef>
#include <cstdint>
#include <cstdio>

#include <log/log.hpp>
#include <pci/pci.hpp>
#include <usb/xhci.hpp>

#include "console.hpp"
#include "fonts.hpp"
#include "graphics.hpp"

uint8_t g_pixel_writer_buf[sizeof(RgbResv8BitPerColorPixelWriter)];

/**
 * @brief ピクセル描画
 * @param[in] config
 * @param[in] x
 * @param[in] y
 * @param[in] c
 * @return 成功時0, 失敗時 負の値
 */
int WritePixel(const FrameBufferConfig &config, int x, int y, const PixelColor &c)
{
	const int pixel_position = config.pixels_per_scan_line * y + x;
	uint8_t *p = &config.frame_buffer[4 * pixel_position];

	switch (config.pixel_format) {
	case PixelFormat::ePixelRgbResv8BitPerColor:
		p[0] = c.r;
		p[1] = c.g;
		p[2] = c.b;
		break;
	case PixelFormat::ePixelBgrResv8BitPerColor:
		p[0] = c.b;
		p[1] = c.g;
		p[2] = c.r;
		break;
	case PixelFormat::ePixelBitMask:    // @todo 未実装
	case PixelFormat::ePixelBltOnly:    // @todo 未実装
	default:
		return -1;
	}

	return 0;
}

#if 0
constexpr int kMouseCursorWidth = 15;
constexpr int kMouseCursorHeight = 24;

	"@              ",    //
	"@@             ",    //
	"@.@            ",    //
	"@..@           ",    //
	"@...@          ",    //
	"@....@         ",    //
	"@.....@        ",    //
	"@......@       ",    //
	"@.......@      ",    //
	"@........@     ",    //
	"@.........@    ",    //
	"@..........@   ",    //
	"@...........@  ",    //
	"@............@ ",    //
	"@.......@@@@@@@",    //
	"@.......@      ",    //
	"@...@....@     ",    //
	"@..@  @..@     ",    //
	"@.@    @..@    ",    //
	"@@     @..@    ",    //
	"@       @..@   ",    //
	"        @..@   ",    //
	"         @.@   ",    //
	"         @@@   ",    //
};
#endif

extern "C" void KernelMain(FrameBufferConfig &frame_buffer_config)
{
	PixelWriter *pixel_writer;

	switch (frame_buffer_config.pixel_format) {
	case PixelFormat::ePixelRgbResv8BitPerColor:
		pixel_writer = new (g_pixel_writer_buf) RgbResv8BitPerColorPixelWriter{frame_buffer_config};
		break;
	case PixelFormat::ePixelBgrResv8BitPerColor:
		pixel_writer = new (g_pixel_writer_buf) BgrResv8BitPerColorPixelWriter{frame_buffer_config};
		break;
	case PixelFormat::ePixelBitMask:    // @todo 未実装
	case PixelFormat::ePixelBltOnly:    // @todo 未実装
	default:
		for (;;) {
			;
		}
		/* NOTREACHED */
	}

	FillRectangle(*pixel_writer, {0, 0},
		      {static_cast<int>(frame_buffer_config.horizontal_resolution),
		       static_cast<int>(frame_buffer_config.vertical_resolution)},
		      {0, 0, 0});

	Console::init(*pixel_writer, {220, 220, 220}, {0, 0, 0});
	auto &console = Console::get_inst();

	Log::init(console);
	auto &log = Log::get_inst();

	log.set_log_level(LogLevel::DEBUG);

	log.log(LogLevel::INFO, "booting...\n");

#if 0
	for (int i = 0; i < 27; i++) {
		printk("test: %d\n", i);
	}

	for (uint32_t x = 0; x < frame_buffer_config.horizontal_resolution; x++) {
		for (uint32_t y = 0; y < frame_buffer_config.vertical_resolution; y++) {
			pixel_writer->Write(x, y, {255, 255, 255});
		}
	}

	for (uint32_t x = 0; x < 400; x++) {
		for (uint32_t y = 0; y < 100; y++) {
			pixel_writer->Write(x, y, {200, 200, 200});
		}
	}

	WriteString(*pixel_writer, 50, 50, "Hello, World!", {0, 0, 0});

	for (int dy = 0; dy < kMouseCursorHeight; dy++) {
		for (int dx = 0; dx < kMouseCursorWidth; dx++) {
			switch (mouse_cursor_shape[dy][dx]) {
			case '@':
				pixel_writer->Write(200 + dx, 100 + dy, {255, 255, 255});
				break;
			case '.':
				pixel_writer->Write(200 + dx, 100 + dy, {0, 0, 0});
				break;
			default:
				break;
			}
		}
	}
#endif

	auto &pci = PciDriver::get_inst();
	pci.scan_all_bus();

	auto xhci = pci.search_by_class(0x0cu, 0x03u, 0x30u);

	if (xhci == nullptr) {
		log.log(LogLevel::INFO, "xHCI device was not found\n");
	}

	log.log(LogLevel::DEBUG, "xhci vendor_id: 0x%04x, mmio_base: 0x%016" PRIx64 "\n", xhci->get_vendor_id(),
		xhci->get_base_addr(0));

	auto xhci_dev = usb::XhciDevice(xhci->get_base_addr(0));

	xhci_dev.reset();
	// xhci_dev.debug();

	for (;;) {
		__asm__("hlt");
	}
}
