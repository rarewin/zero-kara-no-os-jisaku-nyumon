#ifndef FRAME_BUFFER_CONFIG_H_INCLUDED
#define FRAME_BUFFER_CONFIG_H_INCLUDED

#include <stdint.h>

enum class PixelFormat {
	ePixelRgbResv8BitPerColor,
	ePixelBgrResv8BitPerColor,
	ePixelBitMask,	  //< unsupported
	ePixelBltOnly,	  //< unsupported
};

struct FrameBufferConfig {
	uint8_t *frame_buffer;
	uint32_t pixels_per_scan_line;
	uint32_t horizontal_resolution;
	uint32_t vertical_resolution;
	PixelFormat pixel_format;
};
#endif
