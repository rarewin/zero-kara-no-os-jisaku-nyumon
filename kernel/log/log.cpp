#include <stdarg.h>
#include <stdio.h>

#include <console.hpp>

#include "log.hpp"

int Log::log(LogLevel log_level, const char *format, ...)
{
	if (log_level <= this->log_level) {
		va_list ap;
		char s[1024];

		va_start(ap, format);
		vsprintf(s, format, ap);
		va_end(ap);

		this->console.put_string(s);
	}

	return 0;
}

void Log::set_log_level(LogLevel log_level)
{
	this->log_level = log_level;
}
