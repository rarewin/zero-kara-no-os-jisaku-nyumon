#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <console.hpp>

enum class LogLevel { EMERGE, ALERT, CRITICAL, ERROR, WARNING, NOTICE, INFO, DEBUG };

class Log
{
      public:
	Log(const Log &) = delete;		 // コピーコンストラクタ
	Log &operator=(const Log &) = delete;	 // コピー代入演算子
	Log(Log &&) = delete;			 // ムーブコンストラクタ
	Log &operator=(Log &&) = delete;	 // ムーブ代入演算子

	static void init(Console &console)
	{
		Log::get_inst_impl(&console);
	}

	static Log &get_inst()
	{
		return Log::get_inst_impl();
	}

	int log(LogLevel log_level, const char *format, ...);

	void set_log_level(LogLevel log_level);

      private:
	Log(Console *console) : console(*console)
	{
	}
	~Log() = default;

	static Log &get_inst_impl(Console *console = nullptr)
	{
		static Log inst{console};
		return inst;
	}

	LogLevel log_level = LogLevel::INFO;
	Console &console;
};
#endif
