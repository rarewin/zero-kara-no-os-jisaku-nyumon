#ifndef FONTS_H_INCLUDED
#define FONTS_H_INCLUDED

#include "graphics.hpp"

void WriteAscii(PixelWriter &writer, int x, int y, char c, const PixelColor &color);

void WriteString(PixelWriter &writer, int x, int y, const char *str, const PixelColor &color);
#endif
