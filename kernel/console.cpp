#include <cstring>

#include "console.hpp"
#include "fonts.hpp"

void Console::put_string(const char *s)
{
	while (*s) {
		if (*s == '\n') {
			newline();
		} else {
			if (this->cursor_column >= kColumns) {
				newline();
			}
			WriteAscii(this->writer, 8 * this->cursor_column, 16 * this->cursor_row, *s, this->fg_color);
			this->buffer[this->cursor_row][this->cursor_column] = *s;
			this->cursor_column++;
		}
		s++;
	}
}

void Console::newline()
{
	this->cursor_column = 0;

	if (this->cursor_row < kRows - 1) {
		this->cursor_row++;
	} else {
		for (int y = 0; y < 16 * kRows; y++) {
			for (int x = 0; x < 16 * kColumns; x++) {
				this->writer.Write(x, y, this->bg_color);
			}
		}

		for (int row = 0; row < kRows - 1; row++) {
			memcpy(this->buffer[row], this->buffer[row + 1], kColumns + 1);
			WriteString(this->writer, 0, 16 * row, this->buffer[row], this->fg_color);
		}

		memset(this->buffer[kRows - 1], '\0', kColumns + 1);
	}
}
