#![no_std]
#![no_main]

use core::panic::PanicInfo;

use reikou::graphics::{FrameBufferConfig, PixelColor, PixelWriter};

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

#[no_mangle]
pub extern "C" fn KernelMain(frame_buffer_config: &mut FrameBufferConfig) -> ! {
    let pixel_write = PixelWriter::new(&frame_buffer_config);

    pixel_write.fill_rectable(
        (100, 200),
        (400, 200),
        &PixelColor {
            r: 0,
            g: 0xff,
            b: 0,
        },
    );

    pixel_write.draw_rectangle(
        (400, 100),
        (200, 400),
        &PixelColor {
            r: 0xff,
            g: 0xff,
            b: 0,
        },
    );

    pixel_write.write_string(
        (0, 100),
        "!().0123456789:abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[]",
        &PixelColor {
            r: 0x0,
            g: 0x0,
            b: 0x0,
        },
    );

    #[allow(clippy::empty_loop)]
    loop {}
}
/// memcpy()
///
/// # Safety
///
/// アドレスチェック等一切してません
#[no_mangle]
pub unsafe extern "C" fn memcpy(dest: *mut u8, src: *const u8, n: usize) -> *mut u8 {
    let mut i = 0;

    while i < n {
        *dest.add(i) = *src.add(i);
        i += 1;
    }

    dest
}
