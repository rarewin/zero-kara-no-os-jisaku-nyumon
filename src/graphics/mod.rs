pub mod fonts;

use fonts::{FONT_DATA, FONT_HEIGHT, FONT_WIDTH};

/// ピクセルのデータフォーマット
#[repr(C)]
#[derive(Clone)]
pub enum PixelFormat {
    /// RGB
    PixelRgbResv8BitPerColor = 0,
    /// BGR
    PixelBgrResv8BitPerColor = 1,
    /// 未サポート
    PixelBitMask,
    /// 未サポート
    PixelBltOnly,
}

/// フレームバッファ設定 (BootLoaderからもらう)
#[repr(C)]
#[derive(Clone)]
pub struct FrameBufferConfig {
    /// アドレス
    frame_buffer: *mut u8,
    /// 1行あたりのピクセル数
    pixels_per_scan_line: u32,
    /// 水平解像度
    horizontal_resolution: u32,
    /// 垂直解像度
    vertical_resolution: u32,
    /// ピクセルフォーマット
    pixel_format: PixelFormat,
}

/// ピクセル色
pub struct PixelColor {
    /// 赤
    pub r: u8,
    /// 緑
    pub g: u8,
    /// 青
    pub b: u8,
}

/// ピクセルを書く
pub struct PixelWriter {
    /// フレームバッファの設定
    fb_config: FrameBufferConfig,
    /// ワード書き込み用の関数
    pub write_pixel: fn(*mut u8, &PixelColor),
}

impl PixelWriter {
    /// PixelWriterを取得する
    ///
    /// # Arguments
    ///
    /// * `fb_config` - フレームバッファの設定
    pub fn new(fb_config: &FrameBufferConfig) -> Self {
        let write_pixel = match fb_config.pixel_format {
            PixelFormat::PixelBgrResv8BitPerColor => |addr: *mut u8, color: &PixelColor| unsafe {
                *addr.add(0) = color.b;
                *addr.add(1) = color.g;
                *addr.add(2) = color.r;
            },
            PixelFormat::PixelRgbResv8BitPerColor => |addr: *mut u8, color: &PixelColor| unsafe {
                *addr.add(0) = color.r;
                *addr.add(1) = color.g;
                *addr.add(2) = color.b;
            },
            _ => todo!(),
        };

        Self {
            write_pixel,
            fb_config: fb_config.clone(),
        }
    }

    /// `pos` に対応するアドレスを取得する
    ///
    /// # Arguments
    ///
    /// * `pos` - ピクセル位置
    unsafe fn get_pixel_addr(&self, pos: (u32, u32)) -> *mut u8 {
        self.fb_config
            .frame_buffer
            .add(((pos.0 + (pos.1 * self.fb_config.pixels_per_scan_line)) * 4) as usize)
    }

    /// `pos` から `size` だけ `color` で矩形を塗り潰す
    ///
    /// # Arguments
    ///
    /// * `pos` - 左上の点
    /// * `size` - 矩形サイズ
    /// * `color` - 矩形色
    pub fn fill_rectable(&self, pos: (u32, u32), size: (u32, u32), color: &PixelColor) {
        unsafe {
            for dy in 0..(size.1) {
                for dx in 0..(size.0) {
                    (self.write_pixel)(self.get_pixel_addr((pos.0 + dx, pos.1 + dy)), &color);
                }
            }
        }
    }

    /// `pos` から `size` の矩形を `color` で画く
    ///
    /// # Arguments
    ///
    /// * `pos` - 左上の点
    /// * `size` - 矩形サイズ
    /// * `color` - 矩形色
    pub fn draw_rectangle(&self, pos: (u32, u32), size: (u32, u32), color: &PixelColor) {
        unsafe {
            for dx in 0..(size.0) {
                (self.write_pixel)(self.get_pixel_addr((pos.0 + dx, pos.1)), &color);
                (self.write_pixel)(
                    self.get_pixel_addr((pos.0 + dx, pos.1 + size.1 - 1)),
                    &color,
                );
            }

            for dy in 0..(size.1) {
                (self.write_pixel)(self.get_pixel_addr((pos.0, pos.1 + dy)), &color);
                (self.write_pixel)(
                    self.get_pixel_addr((pos.0 + size.0 - 1, pos.1 + dy)),
                    &color,
                );
            }
        }
    }

    /// ASCII文字 `c` を `pos` の位置に `color` で描画する
    ///
    /// # Arguments
    ///
    /// * `pos` - 左上の点
    /// * `c`` - キャラクターコード
    /// * `color` - 文字色
    pub fn write_ascii(&self, pos: (u32, u32), c: char, color: &PixelColor) {
        let idx = c as usize;

        if idx >= FONT_DATA.len() {
            return;
        }

        for dy in 0..FONT_HEIGHT {
            for dx in 0..FONT_WIDTH {
                if (FONT_DATA[idx][dy] & (0b10000000 >> dx)) != 0 {
                    unsafe {
                        (self.write_pixel)(
                            self.get_pixel_addr((pos.0 + dx as u32, pos.1 + dy as u32)),
                            &color,
                        );
                    }
                }
            }
        }
    }

    pub fn write_string(&self, pos: (u32, u32), s: &str, color: &PixelColor) {
        for (i, c) in s.chars().enumerate() {
            self.write_ascii((pos.0 + (i * FONT_WIDTH) as u32, pos.1), c, &color);
        }
    }
}
